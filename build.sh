#!/bin/bash

source env.sh

set -x
set -e

log_environment
prepare_dist_dirs "$STEAM_APP_ID_LIST"

# build <project name>
#
pushd "source"
# write build instructions here
# place resulting binaries in directories: '<app_id>/dist'
popd
